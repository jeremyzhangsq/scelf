\section{Preliminary} 
\label{sec:preli}
\begin{definition}[Social Network]\label{def:social_network}
A social network with $n$ individuals and $m$ social ties can be denoted as an edge-weighted graph $G(V,E,W)$, where $V$ is the set of nodes, $|V|=n$, and $E$ is the set of directed relationship, $E \subseteq V \times V $, $|E| = m$, and $W$ is the set of edge-weights corresponding to each edge in $E$. \cite{arora2017debunking}
\end{definition}

%cf. Definition~\ref{def:social_network}.

We use the notations $W(u,v)$ to denote the weight of edge $e(u,v)$. \textbf{In(v)} and \textbf{Out(v)} denote the set of incoming and outgoing neighbours of vertex $v$ respectively.
\par
The objective in influence maximization (IM) is to maximize the spread of information (or influence) in a network through activation of an initial set of k seed nodes. The dynamics of how information spreads in a network is guided by an information diffusion model. To this end, we first define the notions of seed and active nodes. Next, we use these concepts to define the notion of spread of information in a network and the most popular information diffusion models that have been studied in literature.
\begin{definition} [Seed Node]\label{def:seed_node}
A node $v \in V$ that acts as the source of information diffusion in the graph $G(V,E,W)$ is called a seed node. The set of seed nodes is denoted by $S$.\cite{arora2017debunking}
\end{definition}
\begin{definition}[Active Node] \label{def:active_node}
A node $v \in V$ is deemed active if either (1) It is a seed node ($v \in S$) or (2) It receives information under the dynamics of an information diffusion model $I$, from a previously active node $u \in V_a$. Once activated, the node $v$ is added to the set of active nodes $V_a$.\cite{arora2017debunking}
\end{definition}
\subsection{Diffusion Model} \label{sec:diff_model}
Given a seed node $s \in S$ and a graph $G(V,E,W)$, an information diffusion model $I$ defines a step-by-step process for information propagation. Independent Cascade (IC) and Weighted Cascade (WC) are the two most well-studied information diffusion models. For both the IC and WC models, the first step requires a seed node $s \in S$ to be activated and added to the set of active nodes $V_a$.
\begin{definition} [Weighted Cascade]\label{def:wc}
Under the WC model, time unfolds in discrete steps. At any time-step i, each
newly activated node $u \in V_a$ gets one independent attempt to activate each of its outgoing neighbours $v \in $ \textbf{Out(v)} with a probability $p(u,v)=W(u,v)$ and $W(u,v) = \dfrac{1}{|In(v)|}$.\cite{arora2017debunking}
\end{definition}
\begin{definition} [Independent Cascade]\label{def:ic}
Under the LT model,every node $v$ contains an activation threshold $\theta_v$ , which is chosen uniformly at random from the interval [0,1]. Further, LT dictates that the summation of all incoming edge weights is at most 1, i.e., $\sum_{ \forall u\in In(v)} W(u,v) \leq 1$. $v$ gets activated if the sum of weights $W(u,v)$ of all the incoming edges $e(u,v)$ originating from active nodes exceeds the activation threshold $\theta_v$. \cite{arora2017debunking}
\end{definition}
\subsection{Problem Definition}
\label{sec:definition}
\begin{definition} [Spread]\label{def:spread}
Given an information diffusion
model $I$, the spread $\Gamma (S)$ of a set of seed nodes $S$ is defined as the total number of nodes that are active, including both the newly activated nodes and the initially active set $S$, at the end of the information diffusion process. Mathematically, $\Gamma (S) = |V_a|$.\cite{arora2017debunking}
\end{definition}

Since the information diffusion under various models (IC/WC) is defined as a stochastic process the measure of interest is the expected value of spread. The expected value of spread $ \sigma (*) = E[ \Gamma (*)]$ is computed by performing a high number (typically 10,000) of Monte Carlo simulations of the spread function. The goal in IM, is therefore to solve the following problem.

\begin{definition} [Influence Maximization]\label{def:im}
Given an integer value $k$ and a social network $G$, select a set $S$ of $k$ seeds, $S\subseteq V$ and $k = |S|$, such that the expected value of spread $ \sigma (S) = E[ \Gamma (S)]$ is maximized.\cite{arora2017debunking}
\end{definition}
For any greedy algorithm of influence maximization, it is required that the influence spread function $\sigma (*)$ is monotone and submodular to achieve a $(1 − 1/e)$-approximation.\cite{nemhauser1978analysis} Now we introduce this two properties.

\begin{definition} [Monotonicity]
A function $f(*)$ is monotone if $f(S \cup \{v\}) \geq f(S)$ for any set $S$ and any element $v \notin S $ \cite{cheng2013staticgreedy}
\end{definition}
\begin{definition} [Submodularity] 
A function $f(*)$ is submodular if $f(S \cup \{v\})-f(S) \geq f(T \cup \{v\})-f(T) $ when $S \subseteq T$. And we call $f(S \cup \{v\})-f(S)$ as margin gain. \cite{cheng2013staticgreedy}
\end{definition}

\subsection{Related Works}
\label{sec:related}
After Domingo and Richardson \cite{domingos2001mining,richardson2002mining} tried to solve influence maximization problem in probabilistic approach, Kempe et al. \cite{kempe2003maximizing} formulated it as a combinatorial optimization problem which is NP-hard. Also a greedy approximate solution was proposed and Kempe proved it can guarantee a $(1 - 1/\exp)$ approximation ratio. However, because of the inevitable Monte Carlo simulation in spread estimation part, the scalability of Kempe's greedy algorithm was trapped. After the presentation of Kempe's greedy algorithm, many algorithms were proposed. Besides heuristic algorithms, CELF \cite{leskovec2007cost} and CELF++ \cite{goyal2011celf++} use sub-modularity of influence spread to fasten previous greedy algorithm. On the other hand, despite of difference existing, RIS \cite{borgs2014maximizing}, TIM \cite{tang2014influence}, IMM \cite{tang2015influence}, SG \cite{cheng2013staticgreedy} and PMC \cite{ohsaka2014fast} attempt to solve this problem based on bunches of sampled sub-graphs.

\subsubsection{Cost-Effective Lazy Forward}
\label{celf}
The core idea of CELF is based on the submodularity of influence spread: node $v$'s margin gain $v.mg$ in current iteration cannot be greater than that in previous iterations. CELF only cares about if a node remains at heap top after re-evaluation. If so, it will be chosen as next seed node. The CELF algorithm is described in Algorithm~\ref{alg:celf}. From line 8 to line 13, every time we get the heap top vertex and re-calculate its latest margin gain. If its margin gain is larger than best margin gain, then replace best margin gain and push this vertex back to the heap. If they are the same, we take this node as next seed.

\begin{algorithm}[!h]
    \caption{CELF}
    \begin{algorithmic}[1]
    \label{alg:celf}
    \REQUIRE $G(V,E,W)$, $k$
    \ENSURE seed set $S$
        \STATE $H \leftarrow empty $\,$ max $\,$ heap$; $best \leftarrow null $; $S \leftarrow \emptyset$
        \FOR{$v$ in $V$}
        \STATE $v.mg \leftarrow \sigma (\{v\}) $;
        \STATE add $v$ to $H$
        \ENDFOR
        \WHILE{$|S|<k$}
       	\WHILE{$best.mg \neq max_{v \in H}\{v.mg\}$}
       	\STATE $top \leftarrow arg \max_{v \in H}\{v.mg\}$
       	\STATE $top.mg \leftarrow \sigma(S\cup\{top\})-\sigma(S)$
       	\IF{$top.mg>best.mg$}
       	\STATE $best \leftarrow top$   	
       	\ENDIF
       	\STATE update $H$
       	\ENDWHILE
       	\STATE remove $top$ from $H$
       	\STATE $S \leftarrow S \cup \{best\}$;
       	\ENDWHILE
    \end{algorithmic}
\end{algorithm}

\subsubsection{Static Greedy}
\label{sg}
Given an underlying social network $G$ and a positive integer $k$, the StaticGreedy algorithm runs in the following two stages to seek for a seed set $S$ that maximizes the influence
spread $\sigma (S)$:
\begin{itemize}
\item \textbf{1. Static snapshots:} Select a value of $R$, the number of Monte Carlo snapshots, then randomly sample $R$ snapshots from the underlying social network $G$. For each
snapshot, each edge $e(u,v)$ is sampled according to its
associated weight $W(u,v)$;
\item \textbf{2. Greedy selection:} Start from an empty seed set $S$, iteratively add one node a time into $S$ such that the newly
added node provides the largest marginal gain of $S$, which is estimated on the $R$ snapshots. This process continues until $k$ seed nodes are selected.
\end{itemize}

\begin{algorithm}[!h]
    \caption{SG}
    \begin{algorithmic}[1]
    \label{alg:sg}
    \REQUIRE $G(V,E,W)$, $k$,$R$
    \ENSURE seed set $S$
        \STATE $S \leftarrow \emptyset$
        \FOR{$i=1$ to $R$}
        \STATE $G'_i \leftarrow generate \, snapshot \, from \, G	$
        \ENDFOR
        \WHILE{$|S|<k$}
       	\STATE set $s_v = 0$ for all $v \in V $ \ $S$ //$s_v$ stores the influence spread after adding node v
       	\FOR{$i=1$ to $R$}
       	\FOR{all $v\in V$\ $S$}
       	\STATE $s_v += |R(G'_j,S \cup \{v\})|$ //$R(G'_j,S \cup \{v\})$ is the influence spread of $S \cup \{v\}$ in snapshot $G'_j$
       	\ENDFOR
       	\ENDFOR
       	\STATE $S \leftarrow S \cup \{ arg \max_{v \in V \backslash S} \{s_v/R\} \}$
       	\ENDWHILE
    \end{algorithmic}
\end{algorithm}

The StaticGreedy algorithm is formally described in Algorithm~\ref{alg:sg}. Two main differences between this algorithm and existing greedy algorithms include: (1) Monte Carlo simulations are conducted in static snapshot manner, which are sampled before the greedy process of selecting seed nodes, as is shown in line 2 to 4; (2) The same set of snapshots are reused in every iteration to estimate the influence spread $\sigma (S)$, where explains the meaning of "static".
\par
Moreover, a dynamic update strategy is proposed to speed up the traditional static greedy algorithm above. Specifically, when a node $v^*$ is selected as a seed node, we directly discount the marginal gain of other nodes by the marginal gain shared by these nodes and $v^*$ .
\par
For a snapshot $G'_i$ , we use $T(G'_i ,v)$ to denote the set of
nodes which are reachable from $v$ and use $F(G'_i ,v)$ to denote
the set of nodes from which $v$ can be reached. In the first iteration, the marginal gain of $v$ is $|T(G'_i ,v)|$. In our dynamic update strategy, when $v^*$ is selected as a seed node, we find the set $F(G'_i ,w)$ for each node $w \in T(G'_i ,v^*)$. Then, for every $u \in F(G;_i ,w)$, we delete $w$ from $T(G'_i ,u)$. The size of the remained $T(G'_i ,u)$ reflects the marginal gain of $u$ in the next iteration. In this way, we can maintain a dynamically updated marginal gain for each node to avoid calculating the marginal gain from scratch. The detailed implementation of the improved static algorithm, namely SGDU,is given in Algorithm~\ref{alg:sgdu}.

\begin{algorithm}[!h]
    \caption{SGDU}
    \begin{algorithmic}[1]
    \label{alg:sgdu}
    \REQUIRE $G(V,E,W)$, $k$,$R$
    \ENSURE seed set $S$
        \STATE $S \leftarrow \emptyset$
        \FOR{$i=1$ to $R$}
        \STATE $G'_i \leftarrow generate \, snapshot \, from \, G$
        \FOR{each $v \in V$}
        \STATE compute and record $T(G'_i,v)$ and $F(G'_i,v)$
        \STATE $s_v \leftarrow s_v + |T(G'_i,v)|$
        \ENDFOR
        \ENDFOR
        \WHILE{$|S|<k$}
       	\STATE $v^* \leftarrow arg \max_{v \in V \backslash S}\{s_v\}$
       	\STATE $S \leftarrow S \cup \{v^*\}$
       	\FOR{$i=1$ to $R$}
       	\FOR{each node $w \in T(G'_i,v_{seed})$}
       	\FOR{each node $u \in F(G'_i,w)$}
       	\STATE $delete \, w \, from \, T(G'_i,u)$
       	\STATE $s_u \leftarrow s_u -1$
       	\ENDFOR
       	\ENDFOR
       	\ENDFOR
       	\ENDWHILE
    \end{algorithmic}
\end{algorithm}
